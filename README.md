# Simple k8s Workshop

`app` contains a simple Python Flask app and its Dockerfile.

`k8s-manifests` contain manifests for crawl/walk/run in a Kubernetes deployment.

Each directory contains `notes.txt` for assisting with commands and storyboard.
