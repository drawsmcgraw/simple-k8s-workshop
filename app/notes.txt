# build
docker build -t simple-python-flask .

# buildpack build
pack build buildpack-simple-python-flask

# run docker version
docker run -it -p 5000:5000 simple-python-flask

# run buildpack version
docker run -it -p 5000:5000 buildpack-simple-python-flask

# tag
docker tag simple-python-flask:latest harbor.tanzu.tacticalprogramming.com/workshop/simple-python-flask

# push
docker push harbor.tanzu.tacticalprogramming.com/workshop/simple-python-flask



#
# Demonstration
#

Build with Dockerfile. Tag, push.
Build with pack. Tag, push.

(in k8s-manifests directory)
kubectl apply 03-combined....
  - with Dockerfile
  - with pack
(this shows that they are equivalent outputs)

# what are buildpacks? Where do they come from?
pack builder suggest
  - lists some common buildpacks and descriptions

# deployment
we saw kubectl, now let's see 'kapp'.
kapp deploy -a python-app -f 03-combined....
kapp list
kapp delete -a python-app
  - remember that the above isn't possible with 'kubectl' if you've misplaced your manifest


# fancy deployments
ytt -f ytt/values.yml -f 09-templated-deployment.yml
ytt -f ytt/values.yml -f 09-templated-deployment.yml | kapp -y deploy -a python-app -f -

# helm compatibility
(inside the k8s-manifests/helm directory)
helm template kapp-nginx nginx-9.4.1.tgz --values values.yaml | kapp -y deploy -a kapp-nginx -f -
